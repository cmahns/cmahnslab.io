---
layout: page
title: "Blog"
permalink: /blog/
---

Example of a Blog or Statements section

<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{ post.title }}</a>
    </li>
  {% endfor %}
</ul>

