---
layout: post 
title: "Statement on ICE contract ban going into effect"
permalink: /statement-s3361/
tag: ICE
---

This past Friday, Governor Murphy signed S3361/A5207, which means ICE can no longer enter agreements to incarcerate immigrants in New Jersey. After years of working tirelessly among a broad and diverse coalition of immigrant advocates, the North New Jersey chapter of the Democratic Socialists of America (DSA) is pleased to finally see the beginning of the end of ICE detention in New Jersey. 

As democratic socialists, we fight to end immigration detention because U.S. Immigration and Customs Enforcement (ICE) tears apart working class families. It’s an agency that kidnaps, deports, and enacts violence against Black and Brown working class people in particular. To build solidarity and power among the multiracial working class, ICE has got to go. 

ICE was founded as part of a wave of racist and militarist policies implemented in the name of “national security” following September 11, 2001. Since then, its purpose has been to senselessly terrorize and torture immigrants. It is truly unfortunate that New Jersey ever permitted local entities to profit from contracts with this fascistic agency for so long, but we are glad to become the fifth state, and first on the east coast, to ban this type of collaboration with such an irredeemable agency.

Unfortunately, this bill does not end existing ICE contracts in New Jersey. We demand that the county governments of Essex, Hudson, and Bergen immediately end their existing contracts with ICE. It is also imperative to ensure that all available measures are taken to ensure that current ICE detainees in these facilities are released to their communities, not transferred to other ICE facilities elsewhere in the country. 

And while the New Jersey legislature signed S3361/A5207 back in June, Governor Murphy waited months—until after his vacation in Italy—to sign the bill. This delay gave CoreCivic time to extend their contract to detain people for ICE in Elizabeth. We demand that this last-minute extension in Elizabeth be nullified. 

The North New Jersey DSA will continue the fight for immigrant justice. We will keep pushing to definitively end detention in New Jersey, and organize to empower a multiracial working class across national borders. Get involved [here](https://airtable.com/shrDMVH7KxDpWLLPU), and follow us on [social media](https://twitter.com/Immigration_NNJ) to stay in touch.

